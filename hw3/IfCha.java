/*
	Test abstract operation on -
	Test control flow
	Make sure that CHA is used to get the first subclass that implements the function

*/

class Pablo{
	public static void main(String[] s)
	{
		System.out.println(new C().getVal());
	}
}

class A
{
	public int getVal()
	{
		return 1;
	}
}

class B
{
	public int getVal()
	{
	    int i;

        i = 10;
		if(5 < 10)
		{
			i = 1;
		}
		else
		{
			i = i - 10;
		}
		return i;
	}
}

class C extends B
{
	
}