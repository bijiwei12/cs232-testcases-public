// This file attempts to provide a range of tests for inheritance and extension.
// It should also only succeed on 0-CFA compliant systems. RTA and CHA will provide
// more edges than given.
class Main {
	public static void main(String[] args) {
		A a;
		A b;
		X x;
		X z;
		
		a = new A();
		b = new B();
		
		x = a.m(b);
		z = b.m(a);
		
		System.out.println(x.f(0));
	}
}

class A {
	int p;
	public int f(int a) {
		return a;
	}
    public X m(A a) {
		p = a.f(0);
        return new Y();
    }
}
class B extends A {
	public int f(int a) {
		return a+1;
	}
    public X m(A a) {
		p = a.f(1);
        return new Z();
    }	
}

class X {
	public int f(int a) {
		return 0;
	}
}
class Y extends X {}
class Z extends Y {
	public int f(int a) {
		return 1;
	}
}

