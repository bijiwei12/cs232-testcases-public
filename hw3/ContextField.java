//Since we are doing context-sensitive analysis this time, each instance of a class should keep its own class fields. The program would know x.a is positive and y.a is negative. As we only print x.a here, the output will not contain negative numbers.

class ContextField {
	public static void main(String[] s) {
		A x;
		A y;
		int dummy;

		x = new A();
		y = new A();
		dummy = x.setPositive();
		dummy = y.setNegative();
		dummy = x.print();
	}
}

class A {
	int a;

	public int setPositive() {
		a = 1;
		return 0;
	}

	public int setNegative() {
		a = 1;
		a = a - 2;
		return 0;
	}

	public int print() {
		System.out.println(a);
		return 0;
	}
}
